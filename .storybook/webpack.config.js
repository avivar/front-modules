const path = require('path')

module.exports = ({ config, mode }) => {
    config.module.rules.push({
        test: /\.stories\.js?$/,
        loaders: [ require.resolve('@storybook/source-loader') ],
        enforce: 'pre'
    })
    config.module.rules.push({
        test: /\.stories\.mdx?$/,
        loaders: [ require.resolve('babel-loader'), require.resolve('@mdx-js/loader') ],
    })
    config.module.rules.push({
        test: /\.(jpg|png|gif|svg)?$/,
        loaders: [ require.resolve('file-loader') ]
    })
    return config;
}