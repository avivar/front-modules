import { configure, addParameters, addDecorator } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { addReadme } from 'storybook-readme'
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';


addDecorator(addReadme);
addDecorator(withKnobs);
addParameters({
    readme: {
        StoryPreview: ({ children }) => <div style={{ width: '100%' }}>{children}</div>,
        codeTheme: 'github'
    }
    // docs: {
    //   container: DocsContainer,
    //   page: DocsPage,
    // },
});

const req = require.context('../src/', true, /.stories.(js|mdx)$/);

function loadStories(){
    req.keys().forEach(req);
}

configure(loadStories, module);