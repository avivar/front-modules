import React from 'react'
import { shallow } from 'enzyme'
import { Button } from './Button'
import Item from 'antd/lib/list/Item'

describe('Button', () => {
    it('should render button', async () => {
        const wrapper = shallow(<Button title='title'/>);
        expect(wrapper.hasClass('button')).toEqual(true);
        expect(wrapper.html()).toEqual(shallow(<button className='button'>title</button>).html())
    })
})