import React from 'react';
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { Button } from './Button';

storiesOf('Button', module)
.add('Examples', () => {
  return <Button title='Hello Button' onClick={action('clicked Hello Button')}/>
})
