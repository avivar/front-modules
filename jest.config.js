module.exports = {
    testRegex: '(/__tests__/.*|\\.(test|spec))\\.(js)$',
    coverageDirectory: './coverage',
    coverageReporters: [
        'json',
        'lcov',
        'text'
    ],
    moduleDirectories: ['node_modules', 'src'],
    snapshotSerializers: [
        'enzyme-to-json/serializer.js'
    ],
    setupFilesAfterEnv: [
        '<rootDir>/src/enzyme.js'
    ],
    moduleNameMapper:{
        "\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.js",
        "\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/fileMock.js"
   }
}